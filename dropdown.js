

class DropDown extends HTMLElement {
    constructor() {
        super();
        this._selectedValue;
        this._dropdown;
        this._options=["defult_1" , "defult_2" , "defult_3"]; //default options
        this.attachShadow({ mode: "open" });
        this.shadowRoot.innerHTML=` 
        
         <style>
        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: #ddd;}

        .dropdown:hover .dropdown-content {display: block;}

        .dropdown:hover .dropbtn {background-color: #3e8e41;}
    </style>
    
    <h2>Selected Value: <span id="selected-value"></span></h2>

    <div class="dropdown" id="dropdown">
        <button class="dropbtn">Dropdown</button>
        <div class="dropdown-content" id="dropdown-content"> </div>
    </div>
        `;
    }

    connectedCallback() {
        if(this.hasAttribute('options')){
            this._options= JSON.parse( this.getAttribute('options'));
        }
        this._dropdown = this.shadowRoot.querySelector('#dropdown-content');
        this._selectedValue=this.shadowRoot.querySelector('#selected-value');
        this._renderDropdownOptions();
    }

    _renderDropdownOptions(){
        this._options.forEach((item) => {
            let ddItem = document.createElement('a');
            ddItem.addEventListener('click', this._dropdownClick.bind(this));
            ddItem.innerHTML = item;
            this._dropdown.appendChild(ddItem);
        });
    }

    _dropdownClick(event){
        this._selectedValue.innerHTML = event.target.innerHTML;
    }

}

customElements.define("drop-down", DropDown);
